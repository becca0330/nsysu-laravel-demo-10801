<?php

namespace App\Http\Controllers;

use App\Blog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogs = Blog::all();
        return view('blog.index')->with(['blogs' => $blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::id() == null) {
            return redirect('login');
        } else {
            return view('blog.create');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $title = $request->input('title');
        $summary = $request->input('summary');
        $content = $request->input('content');
        $user_id = Auth::id();

        $cover_img_path = "";
        if($request->file('cover_img') != null) {
            logger()->debug('cover img is not null');
            $cover_img_path = $request->file('cover_img')->store('uploads/cover-imgs', 'direct_public');
        }

        $blog = new Blog();
        $blog->title = $title;
        $blog->summary = $summary;
        $blog->content = $content;
        $blog->user_id = $user_id;
        if($cover_img_path != "") $blog->cover_img = $cover_img_path;

        $blog->save();

        return redirect('blogs')->with('success', '成功建立文章');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        if($blog == null) {
            return redirect('blogs')->with('error', '找不到文章');
        }
        return view('blog.detail')->with('blog', $blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        logger()->debug('edit blog with id :' . $id);
        $blog = Blog::find($id);
        $user_id = Auth::id();
        if($blog->user_id != $user_id) {
            return redirect('blogs')->with('error', '你沒有權限編輯這篇文章');
        }
        return view('blog.edit')->with(['blog' => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        logger()->debug('update blog with id :' . $id);

        $title = $request->input('title');
        $summary = $request->input('summary');
        $content = $request->input('content');
        $user_id = Auth::id();

        $blog = Blog::find($id);

        if($blog->user_id != $user_id) {
            return redirect('blogs')->with('error', '你沒有權限編輯這篇文章');
        }

        $cover_img_path = "";
        if($request->file('cover_img') != null) {
            logger()->debug('cover img is not null');
            $cover_img_path = $request->file('cover_img')->store('uploads/cover-imgs', 'direct_public');
        }

        if($title != "") $blog->title = $title;
        if($summary != "") $blog->summary = $summary;
        if($content != "") $blog->content = $content;
        if($cover_img_path != "") $blog->cover_img = $cover_img_path;

        $blog->save();

        return redirect('blogs')->with('success', '成功更新文章');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        logger()->debug('going to delete post with id :' . $id);
        // check if user owns this post
        $user_id = Auth::id();
        $blog = Blog::find($id);
        if($blog->user_id == $user_id) {
            $blog->delete();
            return redirect('/')->with('success', '成功刪除文章');
        } else {
            // throw error msg
            return redirect('/')->with('error', '你沒有權限刪除這篇文章');
        }

    }

    public function apiList(Request $request) {
        $blogs = Blog::orderBy('created_at', 'desc')->with('user')->get();
        return response()->json(
          [
              'success' => 1,
              'blogs' => $blogs
          ]
        );
    }


    public function apiDetail($id) {

        $blog = Blog::where('id', '=', $id)->with('user')->first();

        return response()->json(
            [
                'success' => 1,
                'blog' => $blog
            ]
        );


    }


    public function apiStore(Request $request) {

        $token = $request->input('token');
        $user_id = $request->input('user_id');
        $title = $request->input('title');
        $summary = $request->input('summary');
        $content = $request->input('content');

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'token' => 'required',
            'title' => 'required',
            'content' => 'required',
            'cover_img' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => $validator->errors()
                ]
            );
        }

        $user = User::where('id', '=', $user_id)->where('api_token', '=', $token)->first();
        if($user == null) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => 'invalid user'
                ]
            );
        }

        $cover_img_path = $request->file('cover_img')->store('uploads/cover-imgs', 'direct_public');

        $blog = new Blog();
        $blog->user_id = $user_id;
        $blog->title = $title;
        $blog->summary = $summary;
        $blog->content = $content;
        $blog->cover_img = $cover_img_path;
        $blog->save();

        return response()->json(
            [
                'success' => 1,
                'blog' => $blog
            ]
        );

    }


    public function apiUpdate(Request $request) {

        $token = $request->input('token');
        $user_id = $request->input('user_id');
        $blog_id = $request->input('blog_id');
        $title = $request->input('title');
        $summary = $request->input('summary');
        $content = $request->input('content');

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'token' => 'required',
            'blog_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => $validator->errors()
                ]
            );
        }

        $user = User::where('id', '=', $user_id)->where('api_token', '=', $token)->first();
        if($user == null) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => 'invalid user'
                ]
            );
        }

        $blog = Blog::where('user_id', '=', $user_id)->where('id', '=', $blog_id)->first();
        if($blog == null) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => 'invalid blog'
                ]
            );
        }

        $cover_img_path = "";
        if($request->file('cover_img') != null) {
            $cover_img_path = $request->file('cover_img')->store('uploads/cover-imgs', 'direct_public');
        }


        if($title != null) $blog->title = $title;
        if($summary != null) $blog->summary = $summary;
        if($content != null) $blog->content = $content;
        if($cover_img_path != "") $blog->cover_img = $cover_img_path;

        $blog->save();

        return response()->json(
            [
                'success' => 1,
                'blog' => $blog
            ]
        );

    }

    public function apiDelete(Request $request) {

        $token = $request->input('token');
        $user_id = $request->input('user_id');
        $blog_id = $request->input('blog_id');

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'token' => 'required',
            'blog_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => $validator->errors()
                ]
            );
        }

        $user = User::where('id', '=', $user_id)->where('api_token', '=', $token)->first();
        if($user == null) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => 'invalid user'
                ]
            );
        }

        $blog = Blog::where('user_id', '=', $user_id)->where('id', '=', $blog_id)->first();
        if($blog == null) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => 'invalid blog'
                ]
            );
        }

        $blog->delete();

        return response()->json(
            [
                'success' => 1
            ]
        );



    }

}
