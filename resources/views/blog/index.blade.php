<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Becca's Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="startbootstrap-clean-blog/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="startbootstrap-clean-blog/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="startbootstrap-clean-blog/css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
@include('common.nav')

<!-- Page Header -->
<header class="masthead" style="background-image: url('startbootstrap-clean-blog/img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>Becca's Blog</h1>
                    <span class="subheading">A Blog Theme by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @foreach($blogs as $blog)
                <div class="post-preview">
                    @if(Auth::id() == $blog->user_id)
                        <div class="float-right">
                            <div class="btn-group" role="group">
                                <form action="{{route('blogs.edit', ['blog' => $blog->id])}}" method="get">
                                    <button type="submit" class="btn btn-outline-light"><i class="fa fa-edit text-success"></i></button>
                                </form>
                                <form action="{{route('blogs.destroy', ['blog' => $blog->id])}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-light" onclick="return confirm('文章會被永久刪除，你確定要刪除文章嗎？');"><i class="fa fa-trash text-danger"></i></button>
                                </form>
                            </div>
                        </div>
                    @endif
                    <a href="/blogs/{{$blog->id}}">

                        <h2 class="post-title">
                            {{$blog->title}}
                        </h2>

                        @if($blog->cover_img != null)
                            <div>
                                <img src="/{{$blog->cover_img}}" style="" class="img-fluid img-thumbnail">
                                <p></p>
                            </div>
                        @endif
                        <h3 class="post-subtitle">
                            {{$blog->summary}}
                        </h3>
                    </a>
                    <p class="post-meta">Posted by
                        <a href="#">{{$blog->user->name}}</a> on {{$blog->created_at}}
                    </p>
                </div>
                <hr>

            @endforeach

            <!-- Pager -->
            <div class="clearfix">
                <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
            </div>
        </div>
    </div>
</div>

<hr>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">
                    <li class="list-inline-item">
                        <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
                        </a>
                    </li>
                </ul>
                <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="startbootstrap-clean-blog/vendor/jquery/jquery.min.js"></script>
<script src="startbootstrap-clean-blog/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for this template -->
<script src="startbootstrap-clean-blog/js/clean-blog.min.js"></script>

</body>

</html>
